Для запуска приложения необходим установленый и настроеный Docker.

###Для установки наберите:
  - *git clone https://pse17@bitbucket.org/pse17/hotparts.git*
Рекомендуется заменить список proxy на актуальный в файле hotparts/proxies.txt
###Для запуска наберите:
  - *cd hotparts/hotparts*
  - *docker-compose build*
  - *docker-compose up*

К заполненой базе данных можно подключится с параметрами  user='user', password='qwerty',db='hotparts'