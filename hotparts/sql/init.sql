CREATE TABLE `analog` (
  `id` varchar(32) NULL,
  `analog` varchar(32) NULL,
  UNIQUE KEY `index1` (`id`,`analog`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `category` (
  `id` varchar(9) NULL,
  `name` varchar(150) NULL,
  `parent_category` varchar(9) NULL,
  `images` varchar(60) NULL,
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `commodity` (
  `id` varchar(32) NULL,
  `name` varchar(150) NULL,
  `category` varchar(9) NULL,
  `images` varchar(60) NULL,
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `compat` (
  `id` varchar(32) NULL,
  `category` varchar(9) NULL,
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
