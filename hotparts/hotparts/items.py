# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
import re
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst

def full_url_processor(self, values):
    return [self.context['response'].urljoin(url) for url in values]


def get_id(self, values):
    #Get last digit value in url
    for value in values:
        id = re.search(r'\/(\d+)\/$', value)
        if id: return id.group(1)
        return ''


def strip_name(self, values):
    for value in values:
        return value.strip()


class CommodityItem(scrapy.Item):
    id = scrapy.Field()
    name = scrapy.Field()
    category = scrapy.Field()
    image_urls = scrapy.Field()
    images = scrapy.Field()


class CommodityLoader(ItemLoader):
    id_out = TakeFirst()
    
    name_in = strip_name
    name_out = TakeFirst()
    
    category_in = get_id
    category_out = TakeFirst()
    
    image_urls_in = full_url_processor


class CategoryItem(scrapy.Item):
    id = scrapy.Field()
    name = scrapy.Field()
    parent_category = scrapy.Field()
    images = scrapy.Field()
    image_urls = scrapy.Field()


class CategoryLoader(ItemLoader):
    id_in = get_id
    id_out = TakeFirst()

    parent_category_in = get_id
    parent_category_out = TakeFirst()

    image_urls_in = full_url_processor
    
    name_in = strip_name
    name_out = TakeFirst()

class CompatItem(scrapy.Item):
    '''Товар  подходит для категории'''
    id = scrapy.Field()         #Артикул товара
    category = scrapy.Field()   #ID категории


class CompatibilityLoader(ItemLoader):
    default_output_processor = TakeFirst()


class AnalogueItem(scrapy.Item):
    '''Товары с этими артикулами совместимы'''
    id = scrapy.Field()      #Артикул товара
    analog = scrapy.Field()  #Артикул аналогичного товара



