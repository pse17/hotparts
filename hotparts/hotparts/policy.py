from rotating_proxies.policy import BanDetectionPolicy

class MyPolicy(BanDetectionPolicy):

    def response_is_ban(self, request, response):
        if response.status == 503:
            return True
        if response.status == 200 and not len(response.body):
            return True
        return False
