# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import os
import json
import logging
import pymysql
from pymysql.err import OperationalError
from pymysql.constants.CR import CR_SERVER_GONE_ERROR,  CR_SERVER_LOST, CR_CONNECTION_ERROR
from scrapy.exceptions import CloseSpider
from scrapy.utils.project import get_project_settings
from twisted.enterprise import adbapi
import hotparts.items as items

logger = logging.getLogger()
settings = get_project_settings()

class HotpartsPipeline(object):
    
    def __init__(self):
        super(HotpartsPipeline, self).__init__()
        self.retries = 5
        self.db = adbapi.ConnectionPool(
            'pymysql',
            host=settings.get('HOST'),
            user=settings.get('USER'),
            passwd=settings.get('PASSWD'),                             
            db=settings.get('DB'),
            charset='utf8mb4',
            cp_min=5,
            cp_max=10,
            cp_reconnect = True,
            cursorclass=pymysql.cursors.DictCursor)    


    def close_spider(self, spider):
        #Close connection pool
        self.db.close()


    def process_item(self, item, spider):
        #Save item to database
        retries = self.retries
        while retries:
            try:
                self.db.runInteraction(self._process_item, item).addErrback(self.handle_error)
            except OperationalError as e:
                if e.args[0] in (
                        CR_SERVER_GONE_ERROR,
                        CR_SERVER_LOST,
                        CR_CONNECTION_ERROR,
                ):
                    retries -= 1
                    logger.info('%s %s attempts to reconnect left', e, retries)
                    continue
                logger.exception(e)
            except Exception as e:
                logger.exception(e)
            break
        else:
            raise CloseSpider('Not connect to database')
        return item


    def handle_error(self, e):
        logger.exception(e)


    def _process_item(self, tx, item):
        table = self.table_name(item)
        if 'images' in item:
            item = self.get_only_image_path(item) #Save only path field, drop other
        item_list = item.items()

        data_update = ','.join(['%s="%s"' % (key, value) for key,value in item_list if key != 'id'])
        keys_insert = ','.join([key for key,_ in item_list])
        values_insert = ','.join(['"%s"' % value for key,value in item_list])

        insert = 'INSERT INTO %s (%s) VALUES (%s) ON DUPLICATE KEY UPDATE %s' % \
            (table, keys_insert, values_insert, data_update)
        
        tx.execute(insert)


    def table_name(self, item):
        if isinstance(item, items.CategoryItem):
            return 'hotparts.category'

        elif isinstance(item, items.CommodityItem):
            return 'hotparts.commodity'

        elif isinstance(item, items.CompatItem):
            return 'hotparts.compat'
            
        elif isinstance(item, items.AnalogueItem) :
            return 'hotparts.analog'


    def get_only_image_path(self, item):
            
        # Drop 'image_urls' field. No need to add to the database (IMHO)
        if  'image_urls' in item: del item['image_urls']

        # Get only path field, drop other
        if item['images'] != []:
            item['images'] = item['images'][0].get('path','')
        else:
            item['images'] = ''

        return item
