import re
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors.lxmlhtml import LxmlLinkExtractor
from scrapy.utils.project import get_project_settings
from scrapy.crawler import CrawlerProcess

import hotparts.items as items
import logging

logger = logging.getLogger()

class HotpartsSpyder(CrawlSpider):
    name = 'hotparts'
    allowed_domains = ['hot-parts.ru']
    start_urls = ['https://hot-parts.ru/catalog/']

    rules = (
        Rule(
            LxmlLinkExtractor(restrict_xpaths='//div[@class="product-offers-table"]/table/tr/td[4]/a'),
            callback='parse_commodity'
        ),
        Rule(
            LxmlLinkExtractor(restrict_xpaths='//ul[@class="bx_catalog_tile_ul"]/li/a'),
            callback='parse_category', follow=True
        ),
        Rule(
            LxmlLinkExtractor(restrict_xpaths='//li[@class="l1 parent"]/a[2]'),
            callback='parse_category', follow=True
        ),
    )

    
    def parse_category(self, response):
        #данные по категории
        loader = items.CategoryLoader(item=items.CategoryItem(), response=response)
        loader.add_value('id', response.request.url)
        loader.add_xpath('name', '//div[@id="breadcrumb"]/span/text()')
        loader.add_xpath('parent_category', '//div[@id="breadcrumb"]/a[last()]/@href' )
        loader.add_xpath('image_urls', '//div[@class="product-item-detail-slider-image active"]/img/@src')
        yield loader.load_item()


    def parse_commodity(self, response):
        #Получаем данные по товару
        loader = items.CommodityLoader(item=items.CommodityItem(), response=response)
        
        #Aртикул товара - последнее слово в строке с названием
        comm_id = self.last_word(response.xpath('//div[@id="breadcrumb"]/span/text()').get())
        loader.add_value('id', comm_id)
        
        loader.add_xpath('name', '//div[@id="breadcrumb"]/span/text()')
        loader.add_xpath('category', '//div[@id="breadcrumb"]/a[last()]/@href')
        loader.add_xpath('image_urls', '//div[@class="product-item-detail-slider-image active"]/img/@src')
        yield loader.load_item()
        
        #Товар  подходит для категории
        compat = items.CompatItem()
        categoryes = response.xpath('//div[@class="bx-catalog-element bx-blue"]/div/div[4]/div/a/@href').re(r'/catalog/(\d+)') 
        for category in categoryes:
            compat['id'] = comm_id  #Артикул товара
            compat['category'] = category #ID категории
            yield compat


        #Товары с этими артикулами совместимы
        analogue = items.AnalogueItem()
        analogues = response.xpath('//table[@data-analog-type=5]/tbody/tr')
        for analog in analogues:
            #Артикул товара
            analogue['id'] =  comm_id  
            #Артикул аналогичного товара
            brand = analog.xpath('td[1]/span/text()').get().strip()
            code = re.sub(r'\W+', '', analog.xpath('td[2]/text()').get().strip())
            analogue['analog'] = ' '.join([brand, code])
            yield analogue


    def last_word(self, value):
        #Получаем артикул товара - последнее слово в строке с названием
        value = value.strip()
        last_word = re.search(r'(\S+)$',value)
        if last_word:
            last_word = last_word.group(0)
            return re.sub(r'\W+', '', last_word) #Убираем тире, подчеркивания и пр.
        return ''
